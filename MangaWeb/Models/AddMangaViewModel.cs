﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MangaWeb.Models
{
    // ViewModel pour la page qui ajoute à la fois des mangas et des auteurs
    public class AddMangaViewModel
    {
        // Utilisé pour le formulaire d'ajout de manga
        public MangaFormModel Manga { get; set; }

        // Utilisé pour le formulaire d'ajout d'auteur
        public AuthorFormModel NewAuthor { get; set; }

        // Liste des auteurs pour le menu déroulant
        public IEnumerable<AuthorViewModel> Authors { get; set; }
    }

    // Modèle de formulaire pour un manga
    public class MangaFormModel
    {
        [Required]
        [StringLength(255)]
        public string Title { get; set; }

        public string Synopsis { get; set; }

        [DataType(DataType.Date)]
        public DateTime? PublicationDate { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }

        [Display(Name = "Author")]
        public int? AuthorId { get; set; }
    }

    // Modèle de formulaire pour un auteur
    public class AuthorFormModel
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Biography { get; set; }
    }

    // ViewModel pour un auteur
    public class AuthorViewModel
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
    }
}
