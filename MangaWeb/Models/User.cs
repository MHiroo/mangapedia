﻿namespace MangaWeb.Models
{
    public class User
    {
        // Ajoutez les propriétés selon votre API
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        // Vous pouvez ajouter d'autres propriétés nécessaires pour l'inscription
        public DateTime SignupDate { get; set; }
    }
}
