﻿using System.ComponentModel.DataAnnotations;

public class AuthorViewModel
{
    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    public string Biography { get; set; }

    // Ajoutez d'autres propriétés au besoin, par exemple :
    // public IFormFile Photo { get; set; } pour un téléchargement d'image
}
