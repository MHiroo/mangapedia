﻿using Microsoft.AspNetCore.Mvc;
using MangaWeb.Models;
using MangaWeb.Services;
using System.Threading.Tasks;

namespace MangaWeb.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserService _userService;

        public AccountController(UserService userService)
        {
            _userService = userService;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Remplacer par la logique d'appel à l'API pour la vérification des identifiants
                var success = await _userService.ValidateUserAsync(model.Email, model.Password);
                if (success)
                {
                    // Logique de traitement après une connexion réussie
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Email ou mot de passe incorrect.");
                }
            }
            return View(model);
        }
    }
}
