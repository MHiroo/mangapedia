﻿using Microsoft.AspNetCore.Mvc;
using MangaWeb.Models;
using MangaWeb.Services;
using System.Threading.Tasks;

namespace MangaWeb.Controllers
{
    public class MangaController : Controller
    {
        private readonly MangaService _mangaService;

        public MangaController(MangaService mangaService)
        {
            _mangaService = mangaService;
        }

        // GET: Affiche la vue pour ajouter un Manga et un Auteur
        public async Task<IActionResult> AddMangaAndAuthor()
        {
            var authors = await _mangaService.GetAuthorsAsync();
            var viewModel = new AddMangaViewModel
            {
                Authors = authors, // Peuplez la liste des auteurs pour le menu déroulant
                Manga = new MangaFormModel(), // Initialisez Manga pour éviter les erreurs de référence nulle
                NewAuthor = new AuthorFormModel() // Initialisez NewAuthor pour le formulaire d'ajout d'auteur
            };

            return View(viewModel);
        }

        // POST: Ajoute un Manga à l'API
        [HttpPost]
        public async Task<IActionResult> AddManga(AddMangaViewModel viewModel)
        {
            if (ModelState.IsValid && viewModel.Manga != null)
            {
                var success = await _mangaService.AddMangaAsync(viewModel.Manga);
                if (success)
                {
                    return RedirectToAction("Index"); // Redirigez vers l'index ou une autre page appropriée
                }
            }

            // En cas d'échec, rechargez la liste des auteurs et affichez à nouveau le formulaire
            viewModel.Authors = await _mangaService.GetAuthorsAsync();
            return View("AddMangaAndAuthor", viewModel);
        }

        // POST: Ajoute un Auteur à l'API
        [HttpPost]
        public async Task<IActionResult> AddAuthor(AddMangaViewModel viewModel)
        {
            if (ModelState.IsValid && viewModel.NewAuthor != null)
            {
                var success = await _mangaService.AddAuthorAsync(viewModel.NewAuthor);
                if (success)
                {
                    // Si l'ajout de l'auteur réussit, redirigez pour ajouter un manga pour cet auteur
                    return RedirectToAction("AddMangaAndAuthor");
                }
            }

            // En cas d'échec, rechargez la liste des auteurs et affichez à nouveau le formulaire
            viewModel.Authors = await _mangaService.GetAuthorsAsync();
            return View("AddMangaAndAuthor", viewModel);
        }
    }
}
