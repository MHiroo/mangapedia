﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MangaWeb.Models;

namespace MangaWeb.Services
{
    public class UserService
    {
        private readonly HttpClient _httpClient;
        private readonly string _loginUrl;

        public UserService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            // Récupère l'URL de connexion depuis appsettings.json
            _loginUrl = configuration["ApiBaseUri:Login"];
        }

        public async Task<bool> ValidateUserAsync(string email, string password)
        {
            var loginModel = new LoginViewModel { Email = email, Password = password };
            var json = JsonConvert.SerializeObject(loginModel);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            // Utilise l'URL de connexion récupérée depuis appsettings.json
            var response = await _httpClient.PostAsync(_loginUrl, content);

            return response.IsSuccessStatusCode;
        }

        public async Task CreateUserAsync(User user)
        {
            var userJson = JsonConvert.SerializeObject(user);
            var content = new StringContent(userJson, Encoding.UTF8, "application/json");
            // Assurez-vous que "UsersApiUrl" est également configuré dans appsettings.json ou ajustez selon besoin
            var response = await _httpClient.PostAsync("http://localhost:5127/api/Users", content);
            response.EnsureSuccessStatusCode();
        }
    }
}
