﻿using MangaWeb.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace MangaWeb.Services
{
    public class MangaService
    {
        private readonly HttpClient _httpClient;
        private readonly string _mangasApiUri;
        private readonly string _authorsApiUri;

        public MangaService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient;
            _mangasApiUri = configuration["ApiBaseUri:Mangas"];
            _authorsApiUri = configuration["ApiBaseUri:Authors"];
        }

        // Récupère la liste des auteurs pour peupler le menu déroulant
        public async Task<IEnumerable<AuthorViewModel>> GetAuthorsAsync()
        {
            var response = await _httpClient.GetAsync(_authorsApiUri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<AuthorViewModel>>(content);
            }
            return new List<AuthorViewModel>();
        }

        // Envoie les données du manga à l'API pour l'ajout
        public async Task<bool> AddMangaAsync(MangaFormModel manga)
        {
            var json = JsonConvert.SerializeObject(manga);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(_mangasApiUri, content);
            return response.IsSuccessStatusCode;
        }

        // Envoie les données de l'auteur à l'API pour l'ajout
        public async Task<bool> AddAuthorAsync(AuthorFormModel author)
        {
            var json = JsonConvert.SerializeObject(author);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(_authorsApiUri, content);
            return response.IsSuccessStatusCode;
        }
    }
}
